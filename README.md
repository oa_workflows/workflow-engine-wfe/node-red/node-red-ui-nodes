# node-red-ui-nodes
Additional nodes for Node-RED Dashboard

This repository contains a few example extra Dashboard Widgets for the [Node-RED
Dashboard](https://flows.nodered.org/node/node-red-dashboard) project - along with documentation that should be enough to help you create your own. In order to work they require both [Node-RED](https://nodered.org) and Node-RED-Dashboard to be installed.

Most widgets will be published as their own standalone npm packages called node-red-node-ui-*something*, rather than as part of this repo. If you create your own nodes please try to call them node-red-contrib-ui-*something*.

A basic knowledge of Javascript is required, and the UI itself uses Angular version 1 and jQuery, so a knowledge of, or a willingness to learn, some of these would be an advantage.

### Original Repository

This repository is a clone of the official [Node-RED](https://github.com/node-red/node-red) repository. It may contain additional features or modifications that are specific to our use case.

[![Forked from](https://img.shields.io/badge/Forked%20from-Node--RED-blue)](https://github.com/node-red/node-red)
